#!/bin/bash

echo "Note: IMPORTANT: If you have not registered at the gitlab site (http://gitlab.chrystechsystems.com) and been added by an admin to the project, please press [ctrl] + [c] at the same time and talk to an admin on the discord."
echo "If you aren't in the directory you want to use for git, press [ctrl]+[c] together to exit the script and copy it to the location you want it. type cd <full directory> to get to the place you want or ls to see what is in the current folder."

echo "Please enter your user nickname: "
read user
echo "Please enter your team name"
echo "Current team names are: "
echo "development"
echo "art"
echo "sound"

read team


echo "Thanks, What do you want to call the remote repository?"
read remote

echo "Alright, cool... So now I need very specifically the exact username you used for the gitlab: "
read username

echo "Cool, let me set you up."

git init
git remote add origin http://$username@gitlab.chrystechsystems.com/chronologic/project-chronos.git
git pull
git checkout -b $team/$user $remote/master
git commit -am "New branch $team/$user created"
git push -u $remote $team/$user

echo "If there were no errors, you are all set."
