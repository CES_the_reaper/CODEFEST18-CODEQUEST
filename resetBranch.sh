#!/bin/bash
echo "If you setup your repo with a remote (such as origin) please enter it here, otherwise, type the full url: "
read remote
echo "Your git directory is in the form of <team>/<user>, you must adhere to it for this to work correctly";
echo "Please enter your team as it is on the git: "
read team
echo "Please enter your username as it is on the git: "
read user
git rm -r .;
git add .;
git commit -am "$user saving changes before update";
git push $remote $team/$user;
git pull;
git checkout master;
git pull origin master;
git checkout $team/$user;
git commit -am "update to state of master branch";
git push -u $remote $team/$user;
echo "Thank you, your stuff should be up to date."

