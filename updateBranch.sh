#/bin/bash

echo "About to merge master into current branch"

echo "Please enter what you called the remote server upon setup (usually origin): "

read remote

echo "Please enter team name (eg. development): "

read team

echo "Please enter user nickname (as is on your repo name): "

read user

echo "Thank you, logging in and pulling from master"

git pull $remote master

git commit -am "Resolving merge"

git push -u $remote $team/$user

echo "Should be done, thank you..."
