#!/bin/bash

echo "Enter Remote name or url, eg: origin: "
read remote
echo "Enter team name, eg: development: "
read team
echo "Enter user name (as defined by your directory), eg: reaper: "
read user
echo "Enter your commit message please: "
read message
echo
echo
echo

echo "Processing commit and push for $team/$user at $remote"
git add .;
git commit -m "$message";
git push -u $remote $team/$user
echo "Commit and push operation should be complete";
