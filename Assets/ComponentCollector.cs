﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentCollector : MonoBehaviour
{
    public GameObject heldGameObject;
    public GameObject x;
    public GameObject InteractionText;
    public GameObject DropText;
    public GameObject YouWinText;
    // Use this for initialization
    void Start()
    {
        if (InteractionText != null)
        {
            InteractionText.SetActive(false);
        }
        if (DropText != null) {
            DropText.SetActive(false);
        }
        if (YouWinText != null) {
            YouWinText.SetActive(false);
        }
        
    }
    GameObject FindGOCameraIsLookingAt()
    {
        Vector3 Start = Camera.main.transform.position;
        Vector3 Direction = Camera.main.transform.forward;
        float Distance = 3.0f;
        

        RaycastHit Hit;
        bool bHit = Physics.Raycast(Start, Direction, out Hit, Distance);

        if (bHit && Hit.collider.tag.EndsWith("Component") && Hit.collider.gameObject != heldGameObject)
        {
            //.Log("Hit Object");
            return Hit.collider.gameObject;
        }

        return null;
    }
    private void Update()
    {
        x = FindGOCameraIsLookingAt();
        if (x != null)
        {
            if (InteractionText != null)
            {
                InteractionText.SetActive(true);
            }
        }
        else
        {
            if (InteractionText != null)
            {
                InteractionText.SetActive(false);
            }
        }
        if (heldGameObject != null)
        {
            DropText.SetActive(true);
        }
        else {
            DropText.SetActive(false);
        }
    }
    // Update is called once per frame
    void LateUpdate()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {

            Debug.Log(x);
            if (x != null)
            {

                heldGameObject = x;
            }
        }
        if (heldGameObject != null)
        {
            heldGameObject.transform.position = this.transform.position;
            heldGameObject.transform.rotation = this.transform.rotation;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            heldGameObject = null;
        }
        
    }
}
