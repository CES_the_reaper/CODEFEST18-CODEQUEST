﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTimer : MonoBehaviour {

    public int[] timer;
    public Text textDisplay;
    int messageNumber = 1;
    public string[] messages;
    private void Start()
    {
        textDisplay.gameObject.SetActive(true);
       
        
        textDisplay.text = messages[0];
    }
    // Update is called once per frame
    void Update () {
       if (messages.Length >= messageNumber)
        {
            if (timer[messageNumber-1] > 0)
            {
                timer[messageNumber-1]--;
                textDisplay.gameObject.SetActive(true);
            }
            else
            {
                textDisplay.gameObject.SetActive(false);
                /*timer[messageNumber] = 300;*/messageNumber++;
                textDisplay.text = messages[messageNumber - 1];
                
            }
        }
        else {
            textDisplay.gameObject.SetActive(false);
        }

	}
}
