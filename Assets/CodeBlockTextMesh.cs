﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeBlockTextMesh : MonoBehaviour {
    public string CodeBlockText;

	// Use this for initialization
	void Start () {
        GameObject CodeBlockTextMesh = new GameObject();
        TextMesh text = CodeBlockTextMesh.AddComponent<TextMesh>();
        text.text = CodeBlockText;
        text.fontSize = 12;

        text.transform.localEulerAngles += new Vector3(90, 0, 0);
        text.transform.localPosition += new Vector3(0f, 0f, 0f);
        
    }
	
	//// Update is called once per frame
	//void Update () {
        
 //   }
}
