﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkAI : MonoBehaviour {
    public GameObject platform2, platform3, platform4, platform5;
    Vector3 position2, position3, position4, position5;
    public Receptical codeBox1, codeBox2, codeBox3, codeBox4, codeBox5, codeBox6;
    public Light darkBotLight;
	// Use this for initialization
	void Start () {
        position2 = platform2.transform.position;
        position3 = platform3.transform.position;
        position4 = platform4.transform.position;
        position5 = platform5.transform.position;
        platform2.transform.position = new Vector3(platform2.transform.position.x,1.0f,platform2.transform.position.z);
        platform3.transform.position = new Vector3(platform3.transform.position.x, 1.0f, platform3.transform.position.z);
        platform4.transform.position = new Vector3(platform4.transform.position.x, 1.0f, platform4.transform.position.z);
        platform5.transform.position = new Vector3(platform5.transform.position.x, 1.0f, platform5.transform.position.z);
        
    }
	
	// Update is called once per frame
	void Update () {
        if (((codeBox1.castedStartObject && codeBox2.castedStopObject) || (codeBox1.castedStopObject && codeBox2.castedStartObject)) && codeBox3.castedStartObject && codeBox4.castedStopObject && codeBox5.castedStopObject && codeBox6.castedStopObject)
        {
            platform2.transform.position = position2;
            platform3.transform.position = position3;
            platform4.transform.position = position4;
            platform5.transform.position = position5;
            darkBotLight.color = Color.green;
        }
        else {
            platform2.transform.position = new Vector3(platform2.transform.position.x, 1.0f, platform2.transform.position.z);
            platform3.transform.position = new Vector3(platform3.transform.position.x, 1.0f, platform3.transform.position.z);
            platform4.transform.position = new Vector3(platform4.transform.position.x, 1.0f, platform4.transform.position.z);
            platform5.transform.position = new Vector3(platform5.transform.position.x, 1.0f, platform5.transform.position.z);
            darkBotLight.color = Color.red;
        }
	}
}
