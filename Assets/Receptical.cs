﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receptical : MonoBehaviour {
    public GameObject ThisObject;
    public GameObject Rim1;
    public GameObject Rim2;
    public GameObject Rim3;
    public GameObject Rim4;
    public GameObject Controlled;
    //public Vector3 Direction;
    public Vector3 RaycastDirection;
    //public float Distance;
    public Vector3 Translation;
    public float RaycastDistance;
    public GameObject castedStartObject;
    public GameObject castedStopObject;
    public bool startObjectIsSame = false;
    public bool stopObjectIsSame = false;
    public bool startFlag = false;
    public bool stopFlag = false;
    Collider coll;
    Vector3 StartLoc;
    Vector3 DoorStartingLocation;
    public Material consoleRimMaterial;
    // Use this for initialization
    void Start() {
        StartLoc = ThisObject.transform.position;
        DoorStartingLocation = Controlled.transform.position;
        coll = ThisObject.GetComponent<Collider>();
        consoleRimMaterial.SetColor(Shader.PropertyToID("_Color"), Color.blue);
        //Material q = ThisObject.GetComponent<Material>();
        //Material consoleRimMaterial = Rim1.GetComponent<Renderer>().material;
        //Debug.Log("Material: " + Rim1.GetComponent<Renderer>().material.name);
    }

    void OnTriggerEnter(Collider CubeCollider)
    {
        Debug.Log(consoleRimMaterial);
        if (CubeCollider.tag == "StartComponent") {
            Debug.Log("Start Component Seen by Collider");
            castedStartObject = CubeCollider.gameObject;
            //Controlled.transform.position.Set(Controlled.transform.position.x + Direction.x * Distance, Controlled.transform.position.y + Direction.y * Distance, Controlled.transform.position.z + Direction.z * Distance);
            if(!startObjectIsSame) Controlled.transform.Translate(Translation);
            consoleRimMaterial.SetColor(Shader.PropertyToID("_Color"), Color.green);
            startFlag = true;
            startObjectIsSame = true;
            stopObjectIsSame = false;
        }
        if (CubeCollider.tag == "StopComponent") {
            Debug.Log("Stop Component Seen by Collider");
            castedStopObject = CubeCollider.gameObject;
            if(!stopObjectIsSame) Controlled.transform.Translate(-Translation);
            consoleRimMaterial.SetColor(Shader.PropertyToID("_Color"), Color.red);
            stopFlag = true;
            startObjectIsSame = false;
            stopObjectIsSame = true;
        }
        if (CubeCollider.tag == "ResetComponent")
        {
            Controlled.transform.position = DoorStartingLocation;
            stopFlag = false;
            startObjectIsSame = false;
            stopObjectIsSame = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        castedStartObject = null;
        castedStopObject = null;
        startFlag = false;
        stopFlag = false;
        consoleRimMaterial.SetColor(Shader.PropertyToID("_Color"), Color.blue);
    }
}
