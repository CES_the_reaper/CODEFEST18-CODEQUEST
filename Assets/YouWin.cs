﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class YouWin : MonoBehaviour {
    public Canvas hud;
    public GameObject YouWinText;
    public Text x;
    
    
    // Use this for initialization
    bool hasWon = false;
    GameObject player;
    void Start() {

    }
    void OnTriggerEnter(Collider PlayerCollider) {
        //PlayerCollider.attachedRigidbody.useGravity = false;
        YouWinText.SetActive(true);
        player = PlayerCollider.gameObject;
        hasWon = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (hasWon) {
            player.transform.Translate(new Vector3(1, 0, 0));
        }
	}
}
